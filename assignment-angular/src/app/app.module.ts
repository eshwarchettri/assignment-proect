import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {HomeserviceService} from './services/homeservice.service';
import {routing} from './app-router';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {HttpClientModule} from '@angular/common/http';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { EditComponentComponent } from './components/edit-component/edit-component.component';
import { InvoiceComponent } from './components/invoice/invoice.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavBarComponent,
    EditComponentComponent,
    InvoiceComponent
  ],
  imports: [
    BsDatepickerModule.forRoot(),
    ConfirmationPopoverModule.forRoot(),
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    routing,
        HttpClientModule,
  ],
  providers: [HomeserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
