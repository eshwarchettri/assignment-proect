import { Component, OnInit } from '@angular/core';
import { HomeserviceService } from '../../services/homeservice.service';
import { DatePipe } from '@angular/common';
import { PaymentDetails } from '../../model/payment-model';
import {Router} from "@angular/router";
@Component({
  selector: 'app-edit-component',
  templateUrl: './edit-component.component.html',
  styleUrls: ['./edit-component.component.css'],
  providers: [DatePipe]

})
export class EditComponentComponent implements OnInit {
  paymentCodes: any;
  date: any;
  paymentDetail: PaymentDetails = new PaymentDetails();

  constructor(private datePipe:DatePipe,private router:Router,private homeService:HomeserviceService) { }

  ngOnInit() {
    this.paymentCodes = ['NET 15', 'NET 30', 'NET 45', 'NET 90'];
    // this.date = new Date();
    // this.paymentDetail.days = 15;



    let paymentId = window.localStorage.getItem("paymentId");
    if(!paymentId) {
      alert("Invalid action.")
      this.router.navigate(['home']);
      return;
    }
    this.homeService.getPaymentDetailsById(paymentId).subscribe(res=>{
      this.paymentDetail=res;
      this.date = new Date(this.paymentDetail.creationDate);

console.log(this.paymentDetail);
    });
  }
  selectOption(paymentCode: any) {
    this.paymentDetail.paymentCode = paymentCode;
    this.paymentDetail.days = paymentCode.substring(4, 7);
  }

  onSubmit() {
    this.paymentDetail.creationDate = this.datePipe.transform(this.date, "dd-MM-yyyy");
    console.log(this.paymentDetail);
    this.homeService.updatePaymentDetails(this.paymentDetail).subscribe(
      res => {
        this.router.navigate(['home']);
console.log(res);
      },
      error => {
        console.log(error);
      }

    );
  }
}
