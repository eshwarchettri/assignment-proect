import { Component, OnInit } from '@angular/core';
import { HomeserviceService } from '../../services/homeservice.service';
import { DatePipe } from '@angular/common';
import { PaymentDetails } from '../../model/payment-model';
import { Router } from "@angular/router";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DatePipe]

})
export class HomeComponent implements OnInit{
  confirmClicked: boolean = false;
  cancelClicked: boolean = false;
  showtable: boolean = false;
  paymentDetail: PaymentDetails = new PaymentDetails();
  paymentDetails: Array<PaymentDetails> = [];
  paymentCodes: any;
  date: any;

  constructor(private router: Router, private homeService: HomeserviceService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.paymentCodes = ['NET 15', 'NET 30', 'NET 45', 'NET 90'];
    this.date = new Date();
    this.paymentDetail.days = 15;
    this.paymentDetail.paymentCode = this.paymentCodes[0];

    this.homeService.getAllPaymentDetails().subscribe(
      data => {
        this.paymentDetails = data;
        console.log(this.paymentDetails);
        if (this.paymentDetails.length > 0) {
          this.showtable = true;
        }
      }
    );
  }

  selectOption(paymentCode: any) {
    this.paymentDetail.paymentCode = paymentCode;
    this.paymentDetail.days = paymentCode.substring(4, 7);
  }
  onSubmit(el: HTMLElement) {
    this.paymentDetail.creationDate = this.datePipe.transform(this.date, "dd-MM-yyyy");
    console.log(this.paymentDetail);
    this.homeService.savePaymentDetails(this.paymentDetail).subscribe(
      res => {
        if (!this.showtable) {
          this.showtable = true;
        }
        this.paymentDetails.push(res);
        console.log(res);
      },
      error => {
        console.log(error);
      }

    );
    el.scrollIntoView();
  }
  updateDetails(paymentdetail: PaymentDetails) {
    window.localStorage.removeItem("paymentId");
    window.localStorage.setItem("paymentId", paymentdetail.id.toString());
    this.router.navigate(['edit']);
  }
  deleteDetails(paymentdetail: PaymentDetails): void {
    // alert(this.confirmClicked);
    // alert(this.cancelClicked);
    this.homeService.deletePayment(paymentdetail.id).subscribe(data => {
      this.paymentDetails = this.paymentDetails.filter(p => p !== paymentdetail);
    });
    console.log(paymentdetail);
  }
  private msg: string;

}
