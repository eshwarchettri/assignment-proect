import { Component, OnInit } from '@angular/core';
import { HomeserviceService } from '../../services/homeservice.service';
import { InvoiceModel } from '../../model/invoice-model';
@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
  invoices: Array<InvoiceModel> = [];

  constructor(private homeService: HomeserviceService) { }

  ngOnInit() {
    console.log("invoices");
    this.homeService.showInvoices().subscribe(res => { 
this.invoices=res;

    });
  }

}
