import { DateArray } from 'ngx-bootstrap/chronos/types';
import {Deserializable} from "./deserializable.model";

export class PaymentDetails implements Deserializable{
    id:number;
    paymentCode:string;
    description:string;
    days:number;
    reminderdays:number;
    creationDate:string;
    
    deserialize(input: any) {
        Object.assign(this, input);
        return this;
      }
}