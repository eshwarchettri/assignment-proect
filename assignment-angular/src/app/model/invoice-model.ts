export class InvoiceModel {
    private invoiceNumber: string;
    private invoiceDate: any;
    private paymentTerm: string;
    private status: string;
}