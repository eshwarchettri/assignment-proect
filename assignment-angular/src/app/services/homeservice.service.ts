import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../constants/app-const';
import { PaymentDetails } from '../model/payment-model';
import { InvoiceModel } from '../model/invoice-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeserviceService {
  private serverPath: string = AppConst.serverPath;
  constructor(private http: HttpClient) { }



  getAllPaymentDetails(): Observable<PaymentDetails[]> {
    let url = this.serverPath + '/api/paymentDetails';

    return this.http.get<PaymentDetails[]>(url);
  }
  updatePaymentDetails(paymentDetail: PaymentDetails): Observable<PaymentDetails> {
    let url = this.serverPath + '/api/update';
    return this.http.put<PaymentDetails>(url + '/' + paymentDetail.id, paymentDetail);
  }
  getPaymentDetailsById(paymentId: string): Observable<PaymentDetails> {
    let url = this.serverPath + '/api/paymentDetail';
    return this.http.get<PaymentDetails>(url + '/' + paymentId);
  }
  deletePayment(id: number): Observable<PaymentDetails> {
    let url = this.serverPath + '/api/deletePaymentDetail';
    return this.http.delete<PaymentDetails>(url + '/' + id);
  }

  savePaymentDetails(paymentDetails: any): Observable<PaymentDetails> {
    let url = this.serverPath + '/api/saveDetails';
    return this.http.post<PaymentDetails>(url, paymentDetails);
  }

  showInvoices(): Observable<InvoiceModel[]> {
    let url = this.serverPath + '/api/invoices';
    return this.http.get<InvoiceModel[]>(url);
  }

}
