import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { EditComponentComponent } from './components/edit-component/edit-component.component'
import {InvoiceComponent} from './components/invoice/invoice.component';
const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'edit',
        component: EditComponentComponent
    },
    {
        path: 'invoice',
        component:InvoiceComponent
    }
];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);