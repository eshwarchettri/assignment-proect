package com.trimindtech.assignment.payment;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
	@Autowired
	private PaymentRepository paymentRepository;

	public PaymentBean savePaymentDetails(PaymentBean bean) {
		return paymentRepository.save(bean);
	}

	public List<PaymentBean> getAllRecored() {
		// TODO Auto-generated method stub
		return paymentRepository.findAll();
	}

	public void deletePaymentDetail(int id) {
		// TODO Auto-generated method stub
		paymentRepository.deleteById(id);
	}

	public PaymentBean getPaymentDetailById(int id) {
		// TODO Auto-generated method stub
		return paymentRepository.findById(id).orElse(null);
	}

	public PaymentBean updatePaymentDetails(int id, PaymentBean bean) {
		// TODO Auto-generated method stub
		Optional<PaymentBean> paymentOptional = paymentRepository.findById(id);
		if (!paymentOptional.isPresent())
			return null;
		bean.setId(id);
		return paymentRepository.save(bean);
	}

	public List<PaymentBean> getAllPaymentDetails() {
		// TODO Auto-generated method stub
		return paymentRepository.findAll();
	}

}
