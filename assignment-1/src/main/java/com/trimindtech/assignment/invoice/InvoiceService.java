package com.trimindtech.assignment.invoice;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class InvoiceService {

	public List<InvoiceBean> getAllInvoices() {
		// TODO Auto-generated method stub
		List<InvoiceBean> invoiceList=new ArrayList<>();
		LocalDate date=LocalDate.now();
		invoiceList.add(new InvoiceBean("INV-001", date, "NET 30", "PAID"));
		invoiceList.add(new InvoiceBean("INV-002", date.plusDays(2), "NET 30", "PAID"));
		invoiceList.add(new InvoiceBean("INV-003", date.plusDays(3), "NET 45", "UNPAID"));
		invoiceList.add(new InvoiceBean("INV-004", date.plusDays(4), "NET 30", "UNPAID"));
		invoiceList.add(new InvoiceBean("INV-005", date.plusDays(5), "NET 15", "UNPAID"));
		invoiceList.add(new InvoiceBean("INV-006", date.plusDays(6), "NET 90", "PAID"));
		invoiceList.add(new InvoiceBean("INV-007", date.plusDays(8), "NET 30", "UNPAID"));
		invoiceList.add(new InvoiceBean("INV-008", date.plusDays(7), "NET 45", "PAID"));

		return invoiceList;
	}

}
