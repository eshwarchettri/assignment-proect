package com.trimindtech.assignment.invoice;

import java.time.LocalDate;

public class InvoiceBean {
	private String invoiceNumber;
	private LocalDate invoiceDate;
	private String paymentTerm;
	private String status;

	public InvoiceBean(String invoiceNumber, LocalDate invoiceDate, String paymentTerm, String status) {
		super();
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.paymentTerm = paymentTerm;
		this.status = status;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
