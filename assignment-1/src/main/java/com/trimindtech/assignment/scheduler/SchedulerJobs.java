package com.trimindtech.assignment.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class SchedulerJobs {
	 @Autowired
	 private SchedulerService SchedulerService;
	 
	@Scheduled(cron = "0 0 0 1/1 * ?")	 
	public void startScheduler() {
		SchedulerService.startScheduler();
	}

}
